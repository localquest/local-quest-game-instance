
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');
var handlebars = require('handlebars');
var io = require('socket.io');
//documentation page hook
var about = require('./routes/about');


var cons = require('consolidate');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));

app.engine('hbs', cons.handlebars);
app.set('view engine', 'hbs');

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'node_modules')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', about.about);
// app.get('/users', user.list);
app.get('/game', routes.index);

var server = http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

var socket = io.listen(server);
var playerIds = 0;

var players = [];

socket.on('connection', function(socket) {
  // A new player has connected.  Send back a unique id that they'll give to
  // their avatar
  var playerId = playerIds;
  socket.emit('id', playerIds);
  playerIds ++;

  socket.on('newPlayer', function(id) {
    // Tell all the other players that a new player has joined the game, send
    // their id, which will be attached to the player sprite
    socket.broadcast.emit('newPlayer', id);
    // Then return the sender a list of the player ids in circulation
    socket.emit('otherPlayers', players);
    // Finally, add your own id to the player list
    players.push(id);
  });

  socket.on('playerMoved', function(msg) {
    // The message is an object consisting of a player id used to identify the
    // sprite to move, and which direction to move it.
    socket.broadcast.emit('playerMoved', msg);
  });

  socket.on('playerStopped', function(id) {
    // Tell everyone else that a player that previous was moving has now
    // stopped
    //console.log("Player stopped message server!");
    socket.broadcast.emit('playerStopped', id);
  });

  socket.on('playerChat', function(msg) {
    // The message is an object consisting of a player id used to identify the
    // player who spoke, and the message itself
    console.log("Chat: ["+msg.id+"]:"+msg.chatMessage);
    socket.broadcast.emit('playerChat', msg);
  });
});
