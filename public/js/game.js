var game;
var ground;
var monsters;
var otherPlayers;
var playerId;
var isMoving = false;
var direction;
var prevDirection;
var notMoving = true;

// Init the game object
function initGame(id) {
  playerId = id;
  game = new Phaser.Game(
    800,
    400,
    Phaser.AUTO,
    'localquest-game',
    { preload: preload, create: create, update: update }
  );
}

function preload() {
  game.load.image('background','/images/swamp.png');
  //game.load.image('ground', '/images/Dungeon-Ground.png');
  game.load.spritesheet('monster', '/images/baddie.png', 32, 32);
  game.load.spritesheet('dude', '/images/dude.png', 32, 48);
}

function create() {
  // Decloare the physics
  game.physics.startSystem(Phaser.Physics.ARCADE);

  game.world.setBounds(0, 0, 2000, 400);

  // Create the ground and add some physics stuff
  // static height 400 from the image height used for background.
  // credit for background image -
  // http://opengameart.org/content/several-scrolling-backgrounds-and-layerable-runners

  ground= game.add.tileSprite(0, game.world.height - 50, game.world.width, game.world.height, 'ground');
  background= game.add.tileSprite(0, game.world.height - 400,
    game.world.width, game.cache.getImage('background').height, 'background');

  game.physics.arcade.enable(ground);
  ground.enableBody = true;
  ground.body.immovable = true;

  // Now let's add the player
  player = game.add.sprite(100, game.world.height - 150, 'dude');

  player.chat = game.add.text(45, 45, '', {
    font: "16px Verdana",
    fill: "#fff"
  });
  player.localtag = game.add.text(45, 45, 'LOCAL', {
    font: "16px Verdana",
    fontWeight: "bold",
    fill: "#8f8"
  });

  // Tell the camera to follow the player
  game.camera.follow(player);

  // Enable physics on player
  game.physics.arcade.enable(player);

  // Player physics properties.  This give a slight bounce, gravity, and
  // collisions
  player.body.bounce.y = 0.2;
  player.body.gravity.y = 300;
  player.body.collideWorldBounds = true;

  // Two animations, walking left and right
  player.animations.add('left', [0, 1, 2, 3], 10, true);
  player.animations.add('right', [5, 6, 7, 8], 10, true);

  otherPlayers = game.add.group();

  cursors = game.input.keyboard.createCursorKeys();

  // Create monsters.  Evenly space them throughout the world
  monsters = game.add.group();
  var monster;
  for(var i = 0; i < 9; i ++) {
    // Create monster
    monster = monsters.create((i * 200) + 40 + (Math.random() * 100), 250, 'monster');
    //Add animation
    monster.animations.add('left', [0, 1], 10, true);
    monster.animations.add('right', [2, 3], 10, true);
    // Enable physics
    game.physics.enable(monster);
    // Add gravity and such
    // collisions
    monster.body.bounce.y = 0.4;
    monster.body.gravity.y = 300;
    monster.body.collideWorldBounds = true;
    if (i==8)
      {
        monster.body.sprite.anchor.setTo(0, 1);
        monster.body.sprite.scale.x=4;
        monster.body.sprite.scale.y=4;
    }
  }

  // Finally, send a message telling the server that a new player has been
  // created
  socket.emit('newPlayer', playerId);
}

function update() {

  // Enable collisions between player and ground
  game.physics.arcade.collide(player, ground);
  game.physics.arcade.collide(otherPlayers, ground);
  // Enable collisions with monsters
  game.physics.arcade.collide(monsters, ground);

  // Reset the players velocity
  player.body.velocity.x = 0;
  player.chat.x = player.body.x;
  player.chat.y = player.body.y-5;
  player.localtag.x = player.body.x;
  player.localtag.y = player.body.y-23;

  for(var i = 0; i < otherPlayers.length; i ++) {
    otherPlayer = otherPlayers.getAt(i);
    otherPlayer.chat.x = otherPlayer.body.x;
    otherPlayer.chat.y = otherPlayer.body.y-5;
    otherPlayer.localtag.x = otherPlayer.body.x;
    otherPlayer.localtag.y = otherPlayer.body.y-23;

    if (otherPlayer.chatTimeout<new Date().getTime()) {
      otherPlayer.chat.text = "";
    }
  }
  if (player.chatTimeout<new Date().getTime()) {
    player.chat.text = "";
  }

  // poll for keyboard events
  if (cursors.left.isDown) {
    // Move to the left
    isMoving = true;
    notMoving = false;
    direction = "left";
    player.body.velocity.x = -150;
    player.animations.play('left');
  } else if (cursors.right.isDown) {
    // Move to the right
    isMoving = true;
    notMoving = false;
    direction = "right";
    player.body.velocity.x = 150;
    player.animations.play('right');
  } else {
    // stand still
    isMoving = false;
    player.animations.stop();
    player.frame = 4;
  }

  // Allow jump if touching the ground
  if (cursors.up.isDown && player.body.touching.down) {
    player.body.velocity.y = -250;
    isMoving = true;
    notMoving = false;
    direction = "up";
  }

  // If moving, send a message to server saying player is moving.  Otherwise, if
  // player is not moving but a flag is set indicating we haven't told the
  // others yet, then relay the message reset the flag
  if ( isMoving && direction != prevDirection) {
    socket.emit('playerMoved', {
      id: playerId,
      direction: direction ,
      x: player.world.x,
      y: player.world.y
    });
    prevDirection = direction;
  } else if ( !isMoving && !notMoving ) {
    socket.emit('playerStopped', playerId);
    notMoving = true;
  }

  // For monster AI, use a random low-chance number to decide to move, a 50/50
  // chance of moving left or right, and then a random low-chance number to
  // stop moving
  var monster;
  var randOne;
  var randTwo;
  for(var i = 0; i < 9; i ++) {
    // Gen random value
    randOne = Math.random();
    // Get monster at index
    monster = monsters.getAt(i);

    // Check to see if it's playing.  If it is, use random num to decide if to
    // kill animation
    if ( monster.animations.isPlaying ) {
      if ( randOne < 0.01 ) {
        monster.animations.stop();
        monster.frame = 1;
        monster.body.velocity.x = 0;
      }
    }
    // Otherwise, if it isn't moving, use random num to decide whether or not
    // to play animation
    else {
      if ( randOne < 0.01 ) {
        // Use another random roll to decide direction
        randTwo = Math.random();
        if ( randTwo < 0.50) {
          monster.animations.play('left');
          monster.body.velocity.x = -50;
        } else {
          monster.animations.play('right');
          monster.body.velocity.x = 50;
        }
      }
    } // end else
  } // endfor
}
function playerChat(chatMessage) {
  player.chat.text = chatMessage;
  player.chatTimeout=new Date().getTime()+10*1000;
  socket.emit('playerChat', {
    id: playerId,
    chatMessage: chatMessage
  });
}

// Detect for socket.io message indicating player chat
socket.on('playerChat', function(msg) {
  // Iterate through the other players until you find one that matches the
  // id
  var otherPlayer;
  for(var i = 0; i < otherPlayers.length; i ++) {
    otherPlayer = otherPlayers.getAt(i);
    if(otherPlayer.playerId == msg.id) {
      // We found the player that sent the message.  Display the chat text.
      otherPlayer.chat.text = msg.chatMessage;
      // Set this to time out in 10 seconds.
      otherPlayer.chatTimeout=new Date().getTime()+10*1000;
    }
  }
});

// When we get a message that there are other players, push a bunch players
// to the otherPlayers group
socket.on('otherPlayers', function(idList) {
  console.log("other player get!");
  for(var i = 0; i < idList.length; i ++) {
    var newPlayer = otherPlayers.create(100, game.world.height - 150, 'dude');
    newPlayer.playerId = idList[i];
    newPlayer.chat = game.add.text(45, 45, '', {
      font: "16px Verdana",
      fill: "#fff"
    });
    newPlayer.localtag = game.add.text(45, 45, 'LOCAL', {
      font: "16px Verdana",
      fontWeight: "bold",
      fill: "#8f8"
    });

    game.physics.arcade.enable(newPlayer);
    newPlayer.body.bounce.y = 0.2;
    newPlayer.body.gravity.y = 300;
    newPlayer.body.collideWorldBounds = true;

    newPlayer.animations.add('left', [0, 1, 2, 3], 10, true);
    newPlayer.animations.add('right', [5, 6, 7, 8], 10, true);
  }
});

// Detect for socket.io message indicating a new player
socket.on('newPlayer', function(msgId) {
  // Add a new player sprite
  var newPlayer = otherPlayers.create(100, game.world.height - 150, 'dude');
  newPlayer.playerId = msgId;
  newPlayer.chat = game.add.text(45, 45, '', {
    font: "16px Verdana",
    fill: "#fff"
  });
  newPlayer.localtag = game.add.text(45, 45, Math.random()>0.5?'LOCAL':'', {
    font: "16px Verdana",
    fill: "#8f8"
  });

  game.physics.arcade.enable(newPlayer);
  newPlayer.body.bounce.y = 0.2;
  newPlayer.body.gravity.y = 300;
  newPlayer.body.collideWorldBounds = true;

  // Then tell create animations
  newPlayer.animations.add('left', [0, 1, 2, 3], 10, true);
  newPlayer.animations.add('right', [5, 6, 7, 8], 10, true);
});

// Detect for socket.io message indicating a player movement
socket.on('playerMoved', function(msg) {
  // Iterate through the other players until you find one that matches the
  // id
  var otherPlayer;
  for(var i = 0; i < otherPlayers.length; i ++) {
    otherPlayer = otherPlayers.getAt(i);
    if(otherPlayer.playerId == msg.id) {
      // We found the player that sent the message.  Move it depending on the
      // directon sent by the relay
      otherPlayer.x = msg.x;
      otherPlayer.y = msg.y;
      console.log(otherPlayer);

      if (msg.direction == "left" ) {
        otherPlayer.body.velocity.x = -150;
        otherPlayer.animations.play("left");
      } else if (msg.direction == "right") {
        otherPlayer.body.velocity.x = 150;
        otherPlayer.animations.play("right");
      } else if (msg.direction == "up") {
        otherPlayer.body.velocity.y = -250;
      }
    }
  } // endfor
});

// Detect for socket.io indicating lack of movement
socket.on('playerStopped', function(msg) {
  console.log("Player stopped message get!");
  // Once again, iterate through list of other players until you find the one
  // correspndong to included id
  var otherPlayer;
  for(var i = 0; i < otherPlayers.length; i ++) {
    otherPlayer = otherPlayers.getAt(i);
    if(otherPlayer.playerId == msg) {
      // We found the player, now assign an x velocity of 0
      otherPlayer.body.velocity.x = 0;
      otherPlayer.animations.stop();
      otherPlayer.frame = 4;
    }
  } //endfor
});
